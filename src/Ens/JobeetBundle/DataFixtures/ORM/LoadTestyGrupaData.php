<?php
namespace Ens\JobeetBundle\DataFixtures\ORM;
 
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Ens\JobeetBundle\Entity\TestyGrupa;
 
class LoadTestyGroupData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $design = new TestyGrupa();
    $design->setNazwa('Polski');
 
    $programming = new TestyGrupa();
    $programming->setNazwa('Matematyka');
 
    $manager = new TestyGrupa();
    $manager->setNazwa('Fizyka');
 
    $administrator = new TestyGrupa();
    $administrator->setNazwa('IQ');
 
    $em->persist($design);
    $em->persist($programming);
    $em->persist($manager);
    $em->persist($administrator);
 
    $em->flush();
 
    $this->addReference('category-design', $design);
    $this->addReference('category-programming', $programming);
    $this->addReference('category-manager', $manager);
    $this->addReference('category-administrator', $administrator);
    
  }
 
  public function getOrder()
  {
    return 1; // the order in which fixtures will be loaded
  }
}