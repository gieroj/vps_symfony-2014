<?php

namespace Ens\JobeetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 */
class Users
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $username;

    /**
     * @var integer
     */
    private $alias;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $testy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Users
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set alias
     *
     * @param integer $alias
     * @return Users
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return integer 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Add testy
     *
     * @param \Ens\JobeetBundle\Entity\Testy $testy
     * @return Users
     */
    public function addTesty(\Ens\JobeetBundle\Entity\Testy $testy)
    {
        $this->testy[] = $testy;

        return $this;
    }

    /**
     * Remove testy
     *
     * @param \Ens\JobeetBundle\Entity\Testy $testy
     */
    public function removeTesty(\Ens\JobeetBundle\Entity\Testy $testy)
    {
        $this->testy->removeElement($testy);
    }

    /**
     * Get testy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTesty()
    {
        return $this->testy;
    }
    
    public function __toString()
    {
        return (string)$this->getId();
    }
}
