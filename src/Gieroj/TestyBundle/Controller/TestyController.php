<?php

namespace Gieroj\TestyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gieroj\TestyBundle\Entity\Testy;
use Gieroj\TestyBundle\Form\TestyType;

/**
 * Testy controller.
 *
 */
class TestyController extends Controller
{

    /**
     * Lists all Testy entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GierojTestyBundle:Testy')->findAll();

        return $this->render('GierojTestyBundle:Testy:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Testy entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Testy();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('gieroj_testy_show', array('id' => $entity->getId())));
        }

        return $this->render('GierojTestyBundle:Testy:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Testy entity.
     *
     * @param Testy $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Testy $entity)
    {
        $form = $this->createForm(new TestyType(), $entity, array(
            'action' => $this->generateUrl('gieroj_testy_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Testy entity.
     *
     */
    public function newAction()
    {
        $entity = new Testy();
        $form   = $this->createCreateForm($entity);

        return $this->render('GierojTestyBundle:Testy:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Testy entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:Testy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testy entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GierojTestyBundle:Testy:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Testy entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:Testy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testy entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GierojTestyBundle:Testy:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Testy entity.
    *
    * @param Testy $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Testy $entity)
    {
        $form = $this->createForm(new TestyType(), $entity, array(
            'action' => $this->generateUrl('gieroj_testy_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Testy entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:Testy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testy entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('gieroj_testy_edit', array('id' => $id)));
        }

        return $this->render('GierojTestyBundle:Testy:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Testy entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GierojTestyBundle:Testy')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Testy entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('gieroj_testy'));
    }

    /**
     * Creates a form to delete a Testy entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gieroj_testy_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
