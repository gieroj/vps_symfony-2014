<?php

namespace Gieroj\TestyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gieroj\TestyBundle\Entity\TestyGrupa;
use Gieroj\TestyBundle\Form\TestyGrupaType;

/**
 * TestyGrupa controller.
 *
 */
class TestyGrupaController extends Controller
{

    /**
     * Lists all TestyGrupa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GierojTestyBundle:TestyGrupa')->findAll();

        return $this->render('GierojTestyBundle:TestyGrupa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TestyGrupa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TestyGrupa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('gieroj_testy_grupa_show', array('id' => $entity->getId())));
        }

        return $this->render('GierojTestyBundle:TestyGrupa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TestyGrupa entity.
     *
     * @param TestyGrupa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TestyGrupa $entity)
    {
        $form = $this->createForm(new TestyGrupaType(), $entity, array(
            'action' => $this->generateUrl('gieroj_testy_grupa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TestyGrupa entity.
     *
     */
    public function newAction()
    {
        $entity = new TestyGrupa();
        $form   = $this->createCreateForm($entity);

        return $this->render('GierojTestyBundle:TestyGrupa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TestyGrupa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:TestyGrupa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TestyGrupa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GierojTestyBundle:TestyGrupa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TestyGrupa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:TestyGrupa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TestyGrupa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GierojTestyBundle:TestyGrupa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TestyGrupa entity.
    *
    * @param TestyGrupa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TestyGrupa $entity)
    {
        $form = $this->createForm(new TestyGrupaType(), $entity, array(
            'action' => $this->generateUrl('gieroj_testy_grupa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TestyGrupa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GierojTestyBundle:TestyGrupa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TestyGrupa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('gieroj_testy_grupa_edit', array('id' => $id)));
        }

        return $this->render('GierojTestyBundle:TestyGrupa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TestyGrupa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GierojTestyBundle:TestyGrupa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TestyGrupa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('gieroj_testy_grupa'));
    }

    /**
     * Creates a form to delete a TestyGrupa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gieroj_testy_grupa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
