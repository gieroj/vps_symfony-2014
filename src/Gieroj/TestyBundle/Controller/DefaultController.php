<?php

namespace Gieroj\TestyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GierojTestyBundle:Default:index.html.twig', array('name' => $name));
    }
}
