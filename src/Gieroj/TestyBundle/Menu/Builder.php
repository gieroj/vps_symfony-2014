<?php
//namespace Gieroj\TestyBundle\Menu;
//
//use Knp\Menu\FactoryInterface;
//use Symfony\Component\DependencyInjection\ContainerAware;
//
//class Builder extends ContainerAware
//{
//    public function mainMenu(FactoryInterface $factory, array $options)
//    {
//        $menu = $factory->createItem('root');
//
//        $menu->addChild('Home', array('route' => 'gieroj_testy_grupa'));
//        $menu->addChild('About Me', array(
//            'route' => 'gieroj_testy'
//        ));
//        // ... add more children
//
//        return $menu;
//    }
//}

namespace Gieroj\TestyBundle\Menu;

use Knp\Menu\FactoryInterface;

/**
 * An example howto inject a default KnpMenu to the Navbar
 * see also Resources/config/example_menu.yml
 * and example_navbar.yml
 * @author phiamo
 *
 */
class Builder
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'navbar' => true,
        ));

        $layout = $menu->addChild('Layout', array(
            'icon' => 'home',
            'route' => 'gieroj_testy_grupa',
        ));

        $dropdown = $menu->addChild('Forms', array(
            'dropdown' => true,
            'caret' => true,
        ));

        $dropdown->addChild('Examples', array('route' => 'gieroj_testy'));
        $dropdown->addChild('Horizontal', array('route' => 'gieroj_testy_grupa'));
//        $dropdown->addChild('Extended Forms', array('route' => 'mopa_bootstrap_forms_extended'));
//        $dropdown->addChild('Extended Views', array('route' => 'mopa_bootstrap_forms_view_extended'));
//        $dropdown->addChild('Embedded Type Forms', array('route' => 'mopa_bootstrap_forms_embeddedtype'));
//        $dropdown->addChild('Forms Errors', array('route' => 'mopa_bootstrap_forms_errors'));
//        $dropdown->addChild('Help Texts', array('route' => 'mopa_bootstrap_forms_helptexts'));
//        $dropdown->addChild('Choice Fields', array('route' => 'mopa_bootstrap_forms_choices'));
//        $dropdown->addChild('Collections Fields', array('route' => 'mopa_bootstrap_forms_collections'));
//        $dropdown->addChild('Form Tabs', array('route' => 'mopa_bootstrap_forms_tabs'));

        $menu->addChild('Menus & Navbars', array('route' => 'gieroj_testy'));
        $menu->addChild('Macros for components', array('route' => 'gieroj_testy_grupa'));

        return $menu;
    }

    public function createNavbarsSubnavMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'subnavbar' => true,
        ));

        $menu->addChild('Top', array('uri' => '#top'));
        $menu->addChild('Menus', array('uri' => '#menus'));
        $menu->addChild('Navbars', array('uri' => '#navbars'));
        $menu->addChild('Template', array('uri' => '#template'));
        // ... add more children
        return $menu;
    }

    public function createComponentsSubnavMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'subnavbar' => true,
        ));

        $menu->addChild('Top', array('uri' => '#top'));
        $menu->addChild('Flashs', array('uri' => '#flashs'));
        $menu->addChild('Session Flashs', array('uri' => '#session-flashes'));
        $menu->addChild('Labels & Badges', array('uri' => '#labels-badges'));
        // ... add more children
        return $menu;
    }
}