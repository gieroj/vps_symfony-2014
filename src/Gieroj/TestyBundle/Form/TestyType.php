<?php

namespace Gieroj\TestyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TestyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_public')
            ->add('is_payed')
            ->add('re_try')
            ->add('previus')
            ->add('timer')
            ->add('name')
            ->add('tags')
            ->add('testy_grupa')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gieroj\TestyBundle\Entity\Testy'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gieroj_testybundle_testy';
    }
}
