<?php

namespace Gieroj\TestyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestyGrupa
 */
class TestyGrupa
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nazwa;

    /**
     * @var integer
     */
    private $id_rodzica;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $testy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     * @return TestyGrupa
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string 
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set id_rodzica
     *
     * @param integer $idRodzica
     * @return TestyGrupa
     */
    public function setIdRodzica($idRodzica)
    {
        $this->id_rodzica = $idRodzica;

        return $this;
    }

    /**
     * Get id_rodzica
     *
     * @return integer 
     */
    public function getIdRodzica()
    {
        return $this->id_rodzica;
    }

    /**
     * Add testy
     *
     * @param \Gieroj\TestyBundle\Entity\Testy $testy
     * @return TestyGrupa
     */
    public function addTesty(\Gieroj\TestyBundle\Entity\Testy $testy)
    {
        $this->testy[] = $testy;

        return $this;
    }

    /**
     * Remove testy
     *
     * @param \Gieroj\TestyBundle\Entity\Testy $testy
     */
    public function removeTesty(\Gieroj\TestyBundle\Entity\Testy $testy)
    {
        $this->testy->removeElement($testy);
    }

    /**
     * Get testy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTesty()
    {
        return $this->testy;
    }
    
    public function __toString()
    {
      return (string)$this->getId();
    }
}
