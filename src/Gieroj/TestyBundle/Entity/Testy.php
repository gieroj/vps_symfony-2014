<?php

namespace Gieroj\TestyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Testy
 */
class Testy
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $is_public;

    /**
     * @var boolean
     */
    private $is_payed;

    /**
     * @var boolean
     */
    private $re_try;

    /**
     * @var boolean
     */
    private $previus;

    /**
     * @var \DateTime
     */
    private $timer;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $tags;

    /**
     * @var \Gieroj\TestyBundle\Entity\TestyGrupa
     */
    private $testy_grupa;

    /**
     * @var \Gieroj\TestyBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set is_public
     *
     * @param boolean $isPublic
     * @return Testy
     */
    public function setIsPublic($isPublic)
    {
        $this->is_public = $isPublic;

        return $this;
    }

    /**
     * Get is_public
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * Set is_payed
     *
     * @param boolean $isPayed
     * @return Testy
     */
    public function setIsPayed($isPayed)
    {
        $this->is_payed = $isPayed;

        return $this;
    }

    /**
     * Get is_payed
     *
     * @return boolean 
     */
    public function getIsPayed()
    {
        return $this->is_payed;
    }

    /**
     * Set re_try
     *
     * @param boolean $reTry
     * @return Testy
     */
    public function setReTry($reTry)
    {
        $this->re_try = $reTry;

        return $this;
    }

    /**
     * Get re_try
     *
     * @return boolean 
     */
    public function getReTry()
    {
        return $this->re_try;
    }

    /**
     * Set previus
     *
     * @param boolean $previus
     * @return Testy
     */
    public function setPrevius($previus)
    {
        $this->previus = $previus;

        return $this;
    }

    /**
     * Get previus
     *
     * @return boolean 
     */
    public function getPrevius()
    {
        return $this->previus;
    }

    /**
     * Set timer
     *
     * @param \DateTime $timer
     * @return Testy
     */
    public function setTimer($timer)
    {
        $this->timer = $timer;

        return $this;
    }

    /**
     * Get timer
     *
     * @return \DateTime 
     */
    public function getTimer()
    {
        return $this->timer;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Testy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Testy
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set testy_grupa
     *
     * @param \Gieroj\TestyBundle\Entity\TestyGrupa $testyGrupa
     * @return Testy
     */
    public function setTestyGrupa(\Gieroj\TestyBundle\Entity\TestyGrupa $testyGrupa = null)
    {
        $this->testy_grupa = $testyGrupa;

        return $this;
    }

    /**
     * Get testy_grupa
     *
     * @return \Gieroj\TestyBundle\Entity\TestyGrupa 
     */
    public function getTestyGrupa()
    {
        return $this->testy_grupa;
    }

    /**
     * Set user
     *
     * @param \Gieroj\TestyBundle\Entity\User $user
     * @return Testy
     */
    public function setUser(\Gieroj\TestyBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Gieroj\TestyBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function __toString()
    {
      return $this->getName();
    }
}
