<?php

// src/Ens/JobeetBundle/DataFixtures/ORM/LoadUsersData.php
namespace Gieroj\TestyBundle\DataFixtures\ORM;
 
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gieroj\TestyBundle\Entity\User;
 
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
    $user = new User();
    $user->setEmail('giero@gmail.com');
    $user->setPassword('gieroj1pl');
    $user->setUsername('gieroj');
    $user->setAlias('0');
 

 
    $em->persist($user);
 
    $em->flush();
    
    $this->addReference('user', $user);
  }
 
  public function getOrder()
  {
    return 2; // the order in which fixtures will be loaded
  }
}