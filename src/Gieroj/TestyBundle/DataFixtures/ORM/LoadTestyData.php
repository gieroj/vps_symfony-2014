<?php
// src/Ens/JobeetBundle/DataFixtures/ORM/LoadJobData.php
namespace Gieroj\TestyBundle\DataFixtures\ORM;
 
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gieroj\TestyBundle\Entity\Testy;
 
class LoadTestyData extends AbstractFixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $em)
  {
     for($i = 100; $i <= 130; $i++)
    {
      $test = new Testy();
      $test->setIsPublic('1');
      $test->setIsPayed('0');
      $test->setReTry('1');
      $test->setPrevius('1');
      $test->setName('Pierwszy test umyslu_'.$i);
      $test->setTags('malpa, kurczak, żaba');
      $test->setTimer(new \DateTime('2012-10-10'));
      $test->setTestyGrupa($em->merge($this->getReference('category-design')));
      $test->setUser($em->merge($this->getReference('user')));

      $em->persist($test);
    }
    $em->flush();
  }
 
  public function getOrder()
  {
    return 3; // the order in which fixtures will be loaded
  }
}